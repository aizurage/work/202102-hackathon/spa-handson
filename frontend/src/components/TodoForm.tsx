import React from 'react';
import { BackendService } from '../backend/BackendService';
import { Todo } from '../backend/generated-rest-client';
import { useInput } from '../hooks/useInput';
import './TodoForm.css';

interface Props {
  addTodo: (returnedTodo: Todo) => void;
}

export const TodoForm: React.FC<Props> = ({ addTodo }) => {

  const [text, textAttributes, setText] = useInput('');

  const add: React.FormEventHandler<HTMLFormElement> = async (event) => {
    event.preventDefault();
    if (!text) {
      return;
    }
    BackendService.postTodo(text)
      .then(response => addTodo(response));
    setText('');
  }

  return (
    <div className="TodoForm_content">
      <form onSubmit={add} className="TodoForm_form">
        <div className="TodoForm_input">
          <input type="text" {...textAttributes} placeholder="タスクを入力してください" />
        </div>
        <div className="TodoForm_button">
          <button>追加</button>
        </div>
      </form>
    </div>
  );
};